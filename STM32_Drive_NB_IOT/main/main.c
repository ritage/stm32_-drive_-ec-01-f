#include "stm32f10x.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_it.h"
#include "tcp.h"
#include "EC616S.h"
#include "AccessCloud.h"


/*
 * Copyright (c) 2020-2021 AIThinker.yangbin All rights reserved.
 *
 * 本工程只是STM32对接EC616S的驱动demo，仅供参考，不保证商用稳定性,
 * 本工程参考该作者进行移植，连接为https://blog.csdn.net/it_boy__/article/details/71975797
 * 最终解释权归深圳市安信可科技有限公司所有。
 *
 * author     Specter
 */


int main(void){
	delay_init();
	uart3_Init(115200);	//log serial
	EC616S_Init(9600);	//command serial
	while(1)
	{
		EC616S_ALiyun_MQTTClient_Test();//入网ALiyun测试
		//EC616S_OneNET_Client_Test();//入网OneNET测试
	}
}
