#include "tcp.h"
#include "usart.h"
#include "EC616S.h"
#include "delay.h"
#include "stdio.h"
#include "string.h"
#include "stm32f10x.h"

volatile u8 TcpClosedFlag = 0;

void EC616S_STA_TCPClient_Test(void)
{
    u8 res;
	
    char str[100]={0};
    EC616S_AT_Test();
		printf("正在配置EC616S\r\n");
    EC616S_Net_Mode_Choose(STA);
    while(!EC616S_JoinAP(User_EC616S_SSID, User_EC616S_PWD));
    EC616S_Enable_MultipleId ( DISABLE );
    while(!EC616S_Link_Server(enumTCP, User_EC616S_TCPServer_IP, User_EC616S_TCPServer_PORT, Single_ID_0));
    while(!EC616S_UnvarnishSend());
		printf("\r\n配置完成");
    while ( 1 )
    {       
			  sprintf (str,"深圳市安信可科技有限公司" );//格式化发送字符串到TCP服务器
        EC616S_SendString ( ENABLE, str, 0, Single_ID_0 );
        delay_ms(1000);
        if(TcpClosedFlag) //判断是否失去连接
        {
            EC616S_ExitUnvarnishSend(); //退出透传模式
            do
            {
                res = EC616S_Get_LinkStatus();     //获取连接状态
            }   
            while(!res);

            if(res == 4)                     //确认失去连接，重连
            {
                
                
                while (!EC616S_JoinAP(User_EC616S_SSID, User_EC616S_PWD ) );
                while (!EC616S_Link_Server(enumTCP, User_EC616S_TCPServer_IP, User_EC616S_TCPServer_PORT, Single_ID_0 ) );        
            } 
            while(!EC616S_UnvarnishSend());                    
        }
    }   
}
