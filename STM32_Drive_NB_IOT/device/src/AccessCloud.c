#include "AccessCloud.h"
#include "EC616S.h"
#include "delay.h"

/*
*以下参数需要用户自行修改才能测试用过
*/
#define product_key "a1tSqlgckT8"
#define device_name "NB_Light"
#define device_secret "958f0274eeabebb94722fac038f5ac0d"
#define device_ID "0000001"
RF_Switch RFMode = RF_Full_Func;
Attach_Net_Mode NetMode = Register_status;

char * serverMsg_OneNET = "183.230.40.39,5683";
char * NetStatus = "+CEREG:1,1";
char * ObjectID = "9025";
char * ResourceID = "5812";
char * uploadData = "hello";

void EC616S_ALiyun_MQTTClient_Test(void)
{
	char pub_topic[64];
	char sub_topic[64];
	bool stepFlag = 0;\
	memset(pub_topic,0,sizeof(pub_topic));
	memset(sub_topic,0,sizeof(sub_topic));
	sprintf ( pub_topic, "/sys/%s/%s/thing/event/property/post", product_key , device_name );
	sprintf ( sub_topic, "/sys/%s/%s/thing/service/property/set", product_key , device_name );
	printf("正在配置EC616S参数\r\n");
	EC616S_AT_Test();				//AT指令测试
	stepFlag = EC616S_RF_Mode_Choose(RFMode);	//关闭飞行模式
	stepFlag = EC616S_Net_Attached_Mode_Choose(NetMode, stepFlag);	//附着网络
	EC616S_Send_AT_Cmd("AT+CGATT=1","OK",NULL,1000);
	EC616S_Send_AT_Cmd("AT+CGDCONT=3,\"IP\",\"CMNET\"","OK",NULL,1000);
	EC616S_Send_AT_Cmd("AT+CGACT=1","OK",NULL,1000);
	stepFlag = EC616S_Cloud_Select(2,1,stepFlag);		//设置入网阿里云平台，通讯数据选择JSON格式
	stepFlag = EC616S_Join_Aliyun(product_key,device_name,device_secret,stepFlag);		//配置入网阿里云必需的三元组
	stepFlag = EC616S_Open_Client_Link(product_key,stepFlag);		//打开TCP客户端
	stepFlag = EC616S_Link_Server(device_ID,stepFlag);				//连接服务器
	if(stepFlag)
		stepFlag = EC616S_MQTT_Sub(sub_topic);					//向服务器订阅指定主题的消息
	
	printf("\r\nMQTT successfully Flag:%02x", stepFlag);
	while(stepFlag)
	{
		EC616S_MQTT_Pub (pub_topic,"{\"method\":\"thing.service.property.set\",\"id\":\"259061280\",\"params\":{\"powerstate\":0},\"version\":\"1.0.0\"}");		//发布消息到MQTT服务器
		delay_ms(1000);
		EC616S_MQTT_Pub (pub_topic,"{\"method\":\"thing.service.property.set\",\"id\":\"259061280\",\"params\":{\"powerstate\":1},\"version\":\"1.0.0\"}");
		delay_ms(1000);
	}
	if(!stepFlag)
		EC616S_Rst();
}

void EC616S_OneNET_Client_Test(void)
{
	static bool stepFlag = 0;
//	char * uploadData = "hello";
	printf("正在配置EC616S参数\r\n");
	EC616S_AT_Test();				//AT指令测试
	stepFlag = EC616S_RF_Mode_Choose(RFMode);	//关闭飞行模式
	stepFlag = EC616S_Net_Attached_Mode_Choose(NetMode,stepFlag);	//附着网络
	stepFlag = EC616S_Port_Inquire(serverMsg_OneNET,stepFlag);		//查询OneNET入网IP以及端口
	stepFlag = EC616S_Network_Status_Inquire(NetStatus, stepFlag);	//查询当前的网络状态
	stepFlag = EC616S_Create_OneNET_Connection(stepFlag);			//创建OneNET的连接
	stepFlag = EC616S_ADD_Object(ObjectID,2, "11", 2, 1, stepFlag);	//添加对象实例
	stepFlag = EC616S_Register_Request(9600,60,stepFlag);			//向OneNET发送注册请求
	stepFlag = EC616S_Register_Source(sizeof(ResourceID), ResourceID, stepFlag);	//向OneNET注册资源
	while(stepFlag){
		EC616S_Notify(ObjectID, ResourceID, (sizeof(uploadData) + 1),uploadData);		//上传数据到OneNET平台
		delay_ms(5000);
	}
	if(!stepFlag)
		EC616S_Rst();
}


