#include "EC616S.h"
#include "usart.h"
#include "delay.h"
#include <stdarg.h>
#include "stdlib.h"

static char msgID[10] = {0};

struct STRUCT_USART_Fram EC616S_Fram_Record_Struct = { 0 };  //定义了一个数据帧结构体
void EC616S_Init(u32 bound)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(EC616S_RST_Pin_Periph_Clock|EC616S_CH_PD_Pin_Periph_Clock, ENABLE);

	GPIO_InitStructure.GPIO_Pin = EC616S_RST_Pin;             
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;     //复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;    
	GPIO_Init(EC616S_RST_Pin_Port, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = EC616S_CH_PD_Pin;               
	GPIO_Init(EC616S_CH_PD_Pin_Port, &GPIO_InitStructure);

	uart2_Init(bound); 
	EC616S_Rst();
}

//对EC616S模块发送AT指令
// cmd 待发送的指令
// ack1,ack2;期待的响应，为NULL表不需响应，两者为或逻辑关系
// time 等待响应时间
//返回1发送成功， 0失败
bool EC616S_Send_AT_Cmd(char *cmd,char *ack1,char *ack2,u32 time)
{ 
    EC616S_Fram_Record_Struct .InfBit .FramLength = 0; //重新接收新的数据包
    EC616S_USART("%s\r\n", cmd);
    if(ack1==0&&ack2==0)     //不需要接收数据
    {
		return true;
    }
	printf("delay some time to get receive data\r\n");
    delay_ms(time);   //提供足够的时间用于接收数据，（数据接收部分在uart中断usart.h --> USART2_IRQHandler(void) 中完成）
	delay_ms(1000);
    EC616S_Fram_Record_Struct.Data_RX_BUF[EC616S_Fram_Record_Struct.InfBit.FramLength ] = '\0';
		
    printf("EC616S_data:%s",EC616S_Fram_Record_Struct .Data_RX_BUF);
    if(ack1!=0&&ack2!=0)
    {
        return ( ( bool ) strstr ( EC616S_Fram_Record_Struct .Data_RX_BUF, ack1 ) || 	//查询
                         ( bool ) strstr ( EC616S_Fram_Record_Struct .Data_RX_BUF, ack2 ) );
    }
    else if( ack1 != 0 )  //strstr(s1,s2);检测s2是否为s1的一部分，是返回该位置，否则返回false，它强制转换为bool类型了
        return ( ( bool ) strstr ( EC616S_Fram_Record_Struct .Data_RX_BUF, ack1 ) );

    else if( ack2 != 0 )
        return ( ( bool ) strstr ( EC616S_Fram_Record_Struct .Data_RX_BUF, ack2 ) );
	else
		return false;
}


//复位重启
void EC616S_Rst(void)
{
    EC616S_RST_Pin_SetL;
    delay_ms(500); 
    EC616S_RST_Pin_SetH;
}


//发送恢复出厂默认设置指令将模块恢复成出厂设置
void EC616S_AT_Test(void)
{
    char count=0;
    delay_ms(1000); 
    while(count < 10)
    {
		printf("AT Test\r\n");
        if(EC616S_Send_AT_Cmd("AT","OK",NULL,500))
        {
            printf("OK\r\n");
            return;
        }
        ++ count;
    }
}


//EC616S的射频电路的关闭与开启方法
//设置成功返回OK 反之ERROR
bool EC616S_RF_Mode_Choose(RF_Switch RFmode)
{
    switch ( RFmode )
    {
        case RF_Least_Func:
			printf("set RF mode: 0\r\n");
            return EC616S_Send_AT_Cmd ( "AT+CFUN=0", "OK", NULL, 500 );

        case RF_Full_Func:
			printf("set RF mode: 1\r\n");
            return EC616S_Send_AT_Cmd ( "AT+CFUN=1", "OK", NULL, 500 );

        case RF_OFF:
			printf("set RF mode: 4\r\n");	//Close RF
            return EC616S_Send_AT_Cmd ( "AT+CFUN=4", "OK", NULL, 500 );

        default:
			printf("illegle command");
			return false;
    }       
}

//EC616S 附着网络
//EC616S的Attach_Net_Mode附着网络类型
//AT+CEREG=0：禁止附着网络
//设置成功返回OK 反之ERROR
bool EC616S_Net_Attached_Mode_Choose(Attach_Net_Mode Netmode, bool stepSwitch)
{
    if(stepSwitch){
		switch ( Netmode )
		{
			case Register_Forbid:
				printf("Attach_Net_Mode: 0\r\n");
				return EC616S_Send_AT_Cmd ( "AT+CEREG=0", "OK", NULL, 500 );
			case Register_status:
				printf("Attach_Net_Mode: 1\r\n");
				return EC616S_Send_AT_Cmd ( "AT+CEREG=1", "OK", NULL, 500 );
			case Register_status_Location:
				printf("Attach_Net_Mode: 2\r\n");
				return EC616S_Send_AT_Cmd ( "AT+CEREG=2", "OK", NULL, 500 );
			
			case Register_status_Location_failResaon:
				return EC616S_Send_AT_Cmd ( "AT+CEREG=3", "OK", NULL, 500 );
			
			case Register_status_Location_timer:
				return EC616S_Send_AT_Cmd ( "AT+CEREG=4", "OK", NULL, 500 );

			case Register_status_Location_failResaon_timer:
				return EC616S_Send_AT_Cmd ( "AT+CEREG=4", "OK", NULL, 500 );
			
			default:
				printf("illegle command");
				return false;
		}  
	}else
		return false;
}

//EC616S 配置入网云平台		AT+ECMTCFG="cloud",<tcpconnectID>,<cloudtype>,<data type>
//tcpconnectID			MQTT类型入网设置为0
//cloudtype：
//		0 mosquitto平台
//		1 OneNet平台
//		2 阿里云
//		3-255 用户自定义
//data type：
//		阿里云定义如下
//		1 Json 数据
//		2 字符串数据
//设置成功返回OK 反之ERROR
bool EC616S_Cloud_Select( uint8_t cloudType, uint8_t dataType, bool stepSwitch)
{
	if(stepSwitch){
		printf("Log in cloud: 1\r\n");
		char cCmd [20];
		sprintf ( cCmd, "AT+ECMTCFG =\"cloud\",0,%d,%d", cloudType, dataType );
		return EC616S_Send_AT_Cmd( cCmd, "OK", NULL, 1000 );
	}else
		return false;
}


/*			以下内容为入网阿里云的操作			*/
//EC616S 配置入网阿里云三元组		AT+ECMTCFG=”aliauth”,<tcpconnectID>[,“<product_key>”,“<device_name>”,“<device_secret>”]
//triples:
//		product_key
//		device_name
//		device_secret
//设置成功返回OK 反之ERROR
bool EC616S_Join_Aliyun( char * product_key, char * device_name, char * device_secret, bool stepSwitch)
{
	if(stepSwitch){
		char cCmd [100];
		sprintf ( cCmd, "AT+ECMTCFG =\"aliauth\",0,\"%s\",\"%s\",\"%s\"", product_key, device_name ,device_secret);
		return EC616S_Send_AT_Cmd( cCmd, "OK", NULL, 1000 );
	}else
		return false;
}

//EC616S 打开客户端连接		AT+ECMTOPEN=<tcpconnectID>,“<host_name>”,<port>
//入网阿里云的域名为：		${YourProductKey}.iot-as-mqtt.${YourRegionId}.aliyuncs.com，${YourProductKey}即设备ProductKey，${YourRegionId}即地域
//设置成功返回OK 反之ERROR
bool EC616S_Open_Client_Link ( char * product_key , bool stepSwitch)
{
    if(stepSwitch){
		char cStr [80];
		sprintf ( cStr, "AT+ECMTOPEN=0,\"%s.iot-as-mqtt.cn-shanghai.aliyuncs.com\",1883", product_key );
		return EC616S_Send_AT_Cmd ( cStr, "OK", "+ECMTOPEN: 0,0", 2000 );
	}else
		return false;
}


//EC616S 用随机ID号，连接阿里云	AT+ECMTCONN=0, "12345"
//enumE  网络类型
//ip ，服务器IP
//ComNum  服务器端口
//id，连接号，确保通信不受外界干扰
//设置成功返回true，反之fasle
bool EC616S_Link_Server( char * Cline_ID, bool stepSwitch)
{
    if(stepSwitch){
		char cStr [20];
		sprintf ( cStr, "AT+ECMTCONN=0, \"%s\"", Cline_ID );
		return EC616S_Send_AT_Cmd ( cStr, "OK", "+ECMTCONN: 0,0,0", 3000 );
	}else
		return false;
}


//EC616S发布数据
//设置成功返回true， 反之false
bool EC616S_MQTT_Pub ( char * topic,  char * message)
{
	char cStr [200];
	sprintf ( cStr, "AT+ECMTPUB=0,0,0,0,\"%s\",\"%s\"", topic , message );
    return EC616S_Send_AT_Cmd ( cStr, "OK", 0, 1000 );
}


//EC616S订阅数据
//设置成功返回true， 反之false
bool EC616S_MQTT_Sub ( char * topic )
{
	bool ret = 0;
	char cStr [100];
	sprintf ( cStr, "AT+ECMTSUB=0,1,\"%s\",1", topic );
	ret = EC616S_Send_AT_Cmd ( cStr, "OK", "+ECMTSUB: 0,1,0,1", 1000 );
	return ret;
}


/*								以下内容为入网OneNET的一些操作指令						*/
//查询OneNET入网IP以及端口		服务器IP号以及端口号："+MIPLCONFIG: 1,183.230.40.39,5683"
bool EC616S_Port_Inquire(char * confirmMsg, bool stepSwitch)
{
	if(stepSwitch){
		printf("OneNET IP & PORT\r\n");
		char cStr [50];
		sprintf ( cStr, "AT+MIPLCONFIG?" );
		return EC616S_Send_AT_Cmd ( cStr, "OK", confirmMsg, 1500 );
	}else
		return false;
}

//查询当前的网络状态，当且仅当返回“+CEREG:1,1”时表示网络正常，可入网OneNET
bool EC616S_Network_Status_Inquire(char * confirmMsg, bool stepSwitch)
{
	if(stepSwitch){
		printf("OneNET net status\r\n");
		return(EC616S_Send_AT_Cmd("AT+CEREG?","OK",confirmMsg,2000));
	}else
		return false;
}

//创建OneNET的连接，当且仅当返回“+MIPLCREATE: 0”时表示网络正常，可入网OneNET
bool EC616S_Create_OneNET_Connection(bool stepSwitch)
{
	if(stepSwitch){
		printf("create OneNET Cloud mount\r\n");
		return(EC616S_Send_AT_Cmd("AT+MIPLCREATE","OK",NULL,2000));
	}else
		return false;
}

//添加对象实例
//"AT+MIPLADDOBJ"命令需要
//						对象ID号
//						实例个数
//						实例位图（一个字符表示一个实例的位图，0表示不可用，1表示可用）
//						属性个数
//						操作个数
bool EC616S_ADD_Object(char * objectID,int16_t instanceCount, char * instanceBitmap, int16_t attributecount, int16_t actioncount, bool stepSwitch )
{
	if(stepSwitch){
		char cStr [40];
		printf("create oneNET object\r\n");
		sprintf ( cStr, "AT+MIPLADDOBJ=0,%s,%d,\"%s\",%d,%d", objectID, instanceCount, instanceBitmap, attributecount, actioncount );
		return EC616S_Send_AT_Cmd ( cStr, "OK", NULL, 2000 );
	}else
		return false;
}

//向OneNET发送注册请求
//		参数内容：生命周期
//					注册周期的单位(s)
bool EC616S_Register_Request(int16_t lifeCycle, int16_t registerUnit, bool stepSwitch)
{
	char * tempData;
	int i = 0;
	tempData = (char *) malloc(sizeof(char) * 15);
	bool flag = 0;
	if(stepSwitch){
		char cStr [40];
		printf("send oneNET register request\r\n");
		sprintf ( cStr, "AT+MIPLOPEN=0,%d,%d", lifeCycle ,registerUnit);
		flag = EC616S_Send_AT_Cmd ( cStr, "OK", "MIPLOBSERVE:", 3000 );
		if(flag){
			tempData = strstr(EC616S_Fram_Record_Struct .Data_RX_BUF, "MIPLOBSERVE:");		//获取"MIPLOBSERVE"在字符串中的位置返回char指针
			printf("template Data:%s\n",tempData);
			tempData = tempData + sizeof("MIPLOBSERVE:") + 2;
			while(*tempData!=','){
				msgID[i] = *tempData;
				i ++;
				tempData++;
			}
			printf("msgID is:%s\n",msgID);
		}
		return flag;
	}else
		return false;
}

//向OneNET注册资源
//			参数：msgID，信息ID，每次注册分配的msgID均会变
//					resourceIDLen，资源长度
//					resourceID，资源内容
bool EC616S_Register_Source(int16_t resourceIDLen, char * resourceID, bool stepSwitch)
{
	if(stepSwitch){
		char cStr [40];
		sprintf ( cStr, "AT+MIPLDISCOVERRSP=0,%s,1,%d,\"%s\"", msgID, resourceIDLen,  resourceID);
		return EC616S_Send_AT_Cmd ( cStr, "OK", NULL, 2000 );
	}else
		return false;
}

//删除对象，参数：对象ID
bool EC616S_Logout(void)
{
    return(EC616S_Send_AT_Cmd("AT+MIPLCLOSE=0","OK", 0, 1000 ));
}

//发送注销请求
bool EC616S_Delete_Object(char ClientNum, char * objectID)
{
	char cStr [40];
	sprintf ( cStr, "AT+MIPLDELOBJ=%c,%s", ClientNum, objectID);
    return EC616S_Send_AT_Cmd ( cStr, "OK", NULL, 500 );
}

//删除实例，AT+MIPLDELETE
bool EC616S_Delete_instance(int16_t ClientNum)
{
    char cStr [30];
	sprintf ( cStr, "AT+MIPLDELETE=%d", ClientNum );
    return EC616S_Send_AT_Cmd ( cStr, "OK", NULL, 500 );
}

//上传数据到OneNET平台
bool EC616S_Notify(char * objectID, char * resourceID, int16_t valueLen, char * value)
{
    char cStr [30];
	sprintf ( cStr, "AT+MIPLNOTIFY=0,%s,%s,0,%s,1,%d,\"%s\",0,0", msgID,objectID,resourceID,valueLen,value);
    return EC616S_Send_AT_Cmd ( cStr, "OK", NULL, 3000 );
}


static char *itoa( int value, char *string, int radix )
{
    int     i, d;
    int     flag = 0;
    char    *ptr = string;

    /* This implementation only works for decimal numbers. */
    if (radix != 10)
    {
        *ptr = 0;
        return string;
    }

    if (!value)
    {
        *ptr++ = 0x30;
        *ptr = 0;
        return string;
    }

    /* if this is a negative value insert the minus sign. */
    if (value < 0)
    {
        *ptr++ = '-';

        /* Make the value positive. */
        value *= -1;

    }

    for (i = 10000; i > 0; i /= 10)
    {
        d = value / i;

        if (d || flag)
        {
            *ptr++ = (char)(d + 0x30);
            value -= (d * i);
            flag = 1;
        }
    }

    /* Null terminate the string. */
    *ptr = 0;

    return string;

} /* NCL_Itoa */


void USART_printf ( USART_TypeDef * USARTx, char * Data, ... )
{
    const char *s;
    int d;   
    char buf[16];


    va_list ap;
    va_start(ap, Data);

    while ( * Data != 0 )     // 判断数据是否到达结束符
    {                                         
        if ( * Data == 0x5c )  //'\'
        {                                     
            switch ( *++Data )
            {
                case 'r':                                     //回车符
                USART_SendData(USARTx, 0x0d);
                Data ++;
                break;

                case 'n':                                     //换行符
                USART_SendData(USARTx, 0x0a);   
                Data ++;
                break;

                default:
                Data ++;
                break;
            }            
        }

        else if ( * Data == '%')
        {                                     
            switch ( *++Data )
            {               
                case 's':                                         //字符串
                s = va_arg(ap, const char *);
                for ( ; *s; s++) 
                {
                    USART_SendData(USARTx,*s);
                    while( USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET );
                }
                Data++;
                break;

                case 'd':           
                    //十进制
                d = va_arg(ap, int);
                itoa(d, buf, 10);
                for (s = buf; *s; s++) 
                {
                    USART_SendData(USARTx,*s);
                    while( USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET );
                }
                     Data++;
                     break;
                default:
                     Data++;
                     break;
            }        
        }
        else USART_SendData(USARTx, *Data++);
        while ( USART_GetFlagStatus ( USARTx, USART_FLAG_TXE ) == RESET );

    }
}
