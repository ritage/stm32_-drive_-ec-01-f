#include "delay.h"

void delay_init(void)	 
{
	SysTick_Config(SystemCoreClock / 1000);
}								    

		   
void delay_us(uint32_t n)
{		
	uint32_t ticks;
    uint32_t told;
    uint32_t tnow;
    uint32_t tcnt = 0;
    uint32_t reload;
       
	reload = SysTick->LOAD;                
    ticks = n * (SystemCoreClock / 1000000);	 /* 需要的节拍数 */  
    
    tcnt = 0;
    told = SysTick->VAL;             /* 刚进入时的计数器值 */

    while (1)
    {
        tnow = SysTick->VAL;    
        if (tnow != told)
        {    
            /* SYSTICK是一个递减的计数器 */    
            if (tnow < told)
            {
                tcnt += told - tnow;    
            }
            /* 重新装载递减 */
            else
            {
                tcnt += reload - tnow + told;    
            }        
            told = tnow;

            /* 时间超过/等于要延迟的时间,则退出 */
            if (tcnt >= ticks)
            {
            	break;
            }
        }  
    } 
}

//注意MS的范围
//SysTick->LOAD为24位寄存器，所以最大延时为
//nms<=0xffffff*8*1000/SYSCLK
//SYSCLK单位国MHz,nm单位为ms
//对48M条件下,nms<=2796
void delay_ms(u16 nms)
{
	for(int i = 0 ;i <nms; i ++)
	{	
		delay_us(1000);
	}
} 
