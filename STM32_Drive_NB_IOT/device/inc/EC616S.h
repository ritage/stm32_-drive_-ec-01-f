#ifndef __EC616S_H
#define __EC616S_H			   
#include "stm32f10x.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#if defined ( __CC_ARM   )
#pragma anon_unions
#endif

//EC616S模式选择
typedef enum
{
    STA,
    AP,
    STA_AP  
}ENUM_Net_ModeTypeDef;

typedef enum
{
	RF_Least_Func=0,
	RF_Full_Func=1,
	RF_OFF=4
}RF_Switch;

//附着网络
typedef enum{
	Register_Forbid=0,
	Register_status,
	Register_status_Location,
	Register_status_Location_failResaon,
	Register_status_Location_timer,
	Register_status_Location_failResaon_timer
} Attach_Net_Mode;

#define EC616S_RST_Pin          GPIO_Pin_4    //复位管脚
#define EC616S_RST_Pin_Port     GPIOA    //复位 
#define EC616S_RST_Pin_Periph_Clock  RCC_APB2Periph_GPIOA       //复位时钟

#define EC616S_CH_PD_Pin     GPIO_Pin_5   //使能管脚
#define EC616S_CH_PD_Pin_Port     GPIOA   //使能端口
#define EC616S_CH_PD_Pin_Periph_Clock  RCC_APB2Periph_GPIOA                     //使能时钟


#define EC616S_RST_Pin_SetH     GPIO_SetBits(EC616S_RST_Pin_Port,EC616S_RST_Pin)
#define EC616S_RST_Pin_SetL     GPIO_ResetBits(EC616S_RST_Pin_Port,EC616S_RST_Pin)


#define EC616S_CH_PD_Pin_SetH     GPIO_SetBits(EC616S_CH_PD_Pin_Port,EC616S_CH_PD_Pin)
#define EC616S_CH_PD_Pin_SetL     GPIO_ResetBits(EC616S_CH_PD_Pin_Port,EC616S_CH_PD_Pin)


#define EC616S_USART(fmt, ...)  USART_printf (USART2, fmt, ##__VA_ARGS__)    
#define PC_USART(fmt, ...)       printf(fmt, ##__VA_ARGS__)       //这是串口打印函数，串口1，执行printf后会自动执行fput函数，重定向了printf。



#define RX_BUF_MAX_LEN 1024       //最大字节数
extern struct STRUCT_USART_Fram   //数据帧结构体
{
    char Data_RX_BUF[RX_BUF_MAX_LEN];
    union 
    {
        __IO u16 InfAll;
        struct 
        {
            __IO u16 FramLength       :15;                               // 14:0 
            __IO u16 FramFinishFlag   :1;                                // 15 
        }InfBit;
    }; 
	
}EC616S_Fram_Record_Struct;


//初始化和TCP功能函数
void EC616S_Init(u32 bound);
void EC616S_AT_Test(void);
bool EC616S_Send_AT_Cmd(char *cmd,char *ack1,char *ack2,u32 time);
void EC616S_Rst(void);
bool EC616S_RF_Mode_Choose(RF_Switch RFmode);
bool EC616S_Net_Attached_Mode_Choose(Attach_Net_Mode Netmode, bool stepSwitch);
bool EC616S_Cloud_Select(uint8_t cloudType, uint8_t dataType, bool stepSwitch);
bool EC616S_Join_Aliyun(char * product_key, char * device_name, char * device_secret, bool stepSwitch);
bool EC616S_Open_Client_Link ( char * product_key , bool stepSwitch);
bool EC616S_Link_Server( char * Cline_ID, bool stepSwitch);

bool EC616S_MQTT_Pub( char * topic,  char * message);
bool EC616S_MQTT_Sub( char * topic );

bool EC616S_Port_Inquire(char * confirmMsg, bool stepSwitch);
bool EC616S_Network_Status_Inquire(char * confirmMsg, bool stepSwitch);
bool EC616S_Create_OneNET_Connection(bool stepSwitch);
bool EC616S_ADD_Object(char * objectID,int16_t instanceCount, char * instanceBitmap, int16_t attributecount, int16_t actioncount, bool stepSwitch );
bool EC616S_Register_Request(int16_t lifeCycle, int16_t registerUnit,bool stepSwitch);
bool EC616S_Register_Source(int16_t resourceIDLen, char * resourceID, bool stepSwitch);
bool EC616S_Logout(void);
bool EC616S_Delete_Object(char ClientNum, char * objectID);
bool EC616S_Delete_instance(int16_t ClientNum);
bool EC616S_Notify(char * objectID, char * resourceID, int16_t valueLen, char * value);

void USART_printf( USART_TypeDef * USARTx, char * Data, ... );

#endif
