# STM32_Drive_EC-01F

#### 介绍
该工程为STM32F103C8T6通过串口发送AT指令驱动安信可NB-IoT开发板——EC-01F入网阿里云或者OneNET

#### 核心内容
![输入图片说明](https://images.gitee.com/uploads/images/2021/1111/220545_f915ede7_5735193.png "屏幕截图.png")

#### 安装教程

开发环境：Keil uVision 5


#### 使用说明

1.  在AccessCloud.c文件中可配置入网阿里云或者OneNET云平台的各个参数；
2.  main.c主函数中可切换入网的云平台；

